package lv.romans.meters.emailservice

import lv.romans.meters.commons.messaging.Queues
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EmailServiceApplication {

    @RabbitListener(queues = ["user_creation_email"])
    fun listenEvents(userRegistrationJson: String) {
        println(userRegistrationJson)
    }
}

fun main(args: Array<String>) {
    runApplication<EmailServiceApplication>(*args)
}
